<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\pertanyaan;
class postconroller extends Controller
{
    public function index(){
        // $post = new pertanyaan;

        $pertanyaan = pertanyaan::all();
        return view('posts.index', compact('pertanyaan'));
    }

    public function create(){
        return view('posts.create');
    }

    public function store(request $request){
            $request->validate([
                'judul' => 'required|unique:pertanyaan|max:255',
                'isi' => 'required',
                'tanggal_diperbaharui' => 'required',
                'user_id' => 'required'
        ]);


        // $query = DB::table('pertanyaan')->insert([  
        //     "judul" => $request["judul"], 
        //     "isi" => $request["isi"],
        //     "tanggal_diperbaharui"=>$request["tanggal_diperbaharui"],
        //     "user_id" => $request["user_id"]  
        //     ]);

        // $post = new pertanyaan;

        // $post->judul = $request["judul"];
        // $post->isi = $request["isi"];
        // $post->tanggal_diperbaharui = $request["tanggal_diperbaharui"];
        // $post->user_id = $request["user_id"];

        
        // $post->save();
            $pertanyaan = pertanyaan::create([
                "judul" => $request['judul'],
                "isi" =>   $request['isi'],
                "tanggal_diperbaharui" => $request['tanggal_diperbaharui'],
                "user_id" => $request['user_id']

            ]);
        return redirect('/pertanyaan')->with('berhasil','Pertanyaan berhasil disimpan');     
    }

    public function show($id){
        // $pertanyaan = DB::table('pertanyaan')->where('id', $id)->first();
        $pertanyaan = pertanyaan::find($id);
       
        return view('posts.show', compact('pertanyaan'));
    }

    public function edit($id){
        $pertanyaan = DB::table('pertanyaan')->where('id', $id)->first();
       
        return view('posts.edit', compact('pertanyaan'));
    }

    public function update($id, request $request){
        $request->validate([
            'judul' => 'required|max:255',
            'isi' => 'required',
            'tanggal_diperbaharui' => 'required',
            'user_id' => 'required'
    ]);
        
        // $query =  DB::table('pertanyaan')
        // ->where('id', $id)
        // ->update([
                // 'judul' => $request['judul'],
                // 'isi' =>   $request['isi'],
                // 'tanggal_diperbaharui' => $request['tanggal_diperbaharui'],
                // 'user_id' => $request['user_id']

        // ]);

        $post = pertanyaan::find($id);

        $post->judul = $request["judul"];
        $post->isi = $request["isi"];
        $post->tanggal_diperbaharui = $request["tanggal_diperbaharui"];
        $post->user_id = $request["user_id"];

        $post->save();
            return redirect('/pertanyaan')->with('berhasil', 'data berhasil di update!');
    }

    public function destroy($id){
    // $pertanyaan = DB::table('pertanyaan')->where('id', $id)->delete();
    $post = pertanyaan::find($id);
    $post->delete();
    return redirect('/pertanyaan')->with('berhasil', 'data berhasil di dihapus!');
    }


}
