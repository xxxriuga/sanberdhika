<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pertanyaan extends Model
{
    //
    protected $table = 'pertanyaan';
    protected $fillable = ['judul','isi','tanggal_diperbaharui','user_id'];

    public function user(){
        return $this->belongto(User::class);
    }
}
