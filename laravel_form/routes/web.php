<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



use App\Http\Controllers\homecontroller;

// Route admin lte

Route::get('/', 'master@index');
Route::get('/data-table', 'master@data');

// Route::get('master', function () {
//     return view('master');
// });


//route form
Route::get('/home', 'homecontroller@index');
Route::get('/login', 'authcontroller@index');
Route::post('/login', 'authcontroller@login');

Route::resource('pertanyaan', 'postconroller');


