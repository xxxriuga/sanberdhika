<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h1>Buat Account Baru</h1>
    <h2>Sign Up Form</h2>

    <form action="/login" method="POST">
        @csrf
        <label for="fname">First Name: </label><br><br>
        <input type="text" name="username" id="fname">

        <br><br><label for="lname">Last Name: </label><br><br>
        <input type="text" name="" id="lname">

        <br><br>

        <p>Gender: </p>

        <input type="radio" id="male" name="gender" value="male">
        <label for="male">Male</label><br>
        <input type="radio" id="female" name="gender" value="female">
        <label for="female">Female</label><br>
        <input type="radio" id="other" name="gender" value="other">
        <label for="other">Other</label>

        <br><br>

        <label for="negara">Nationallity: </label>
        <br><br>
        <select name="" id="negara">
            <option value="Indonesia">Indonesia</option>
            <option value="Rusia">Rusia</option>
            <option value="selandiabaru">New Zealand</option>
        </select>

        <p>Language Spoke: </p>
        <input type="checkbox" id="indonesia" name="indonesia" value="indonesia">
        <label for="indonesia"> Indonesia</label><br>
        <input type="checkbox" id="english" name="english" value="english">
        <label for="english"> English</label><br>
        <input type="checkbox" id="other1" name="other" value="other">
        <label for="other1"> Other </label>

        <br><br>

        <label for="bio">Bio: </label><br><br>
        <textarea name="" id="bio" cols="30" rows="10"></textarea>

        <br> <br>

        <input type="submit" value="signup" name="" id="">
    </form>


</body>

</html>