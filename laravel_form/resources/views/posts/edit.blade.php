@extends('master')


@section('content')
<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit post pertanyaan</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action ="/pertanyaan/{{$pertanyaan->id}}" method="POST">
                @csrf
                @method('PUT')
                <div class="card-body">
                  <div class="form-group">
                    <label for="judul">Masukan judul</label>
                    <input type="text" name="judul" class="form-control" id="judul" placeholder="Enter Judul" value="{{old('judul', $pertanyaan->judul)}}">
                    @error('judul')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                 </div>
                  <div class="form-group">
                    <label for="isi">Masukan isi</label>
                    <input type="text" name="isi" class="form-control" id="isi" placeholder="Enter Isi" value="{{old('isi', $pertanyaan->isi)}}">
                    @error('isi')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                  <label for="tanggalperbaharui">Tanggal diperbaharui</label>
                  <input type="datetime-local" name="tanggal_diperbaharui" class="form-control" id="tanggalperbaharui" placeholder="Password" value="{{old('tanggal_diperbaharui', $pertanyaan->tanggal_diperbaharui)}}">
                  @error('tanggal_diperbaharui')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="pertanyaan_id">Masukan id username anda</label>
                    <input type="text" class="form-control" id="pertanyaan_id" placeholder="id username" name="pertanyaan_id" value="{{old('pertanyaan_id', $pertanyaan->pertanyaan_id)}}">
                    @error('pertanyaan_id')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
@endsection