@extends('master')


@section('content')
    <div class="card">
              <div class="card-header">
                <h3 class="card-title">Table pertanyaan</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
              @if(session('berhasil'))
                <div class="alert alert-success">
                    {{session('berhasil')}}
                </div>
              @endif
                <a name="" id="" class="btn btn-primary" href="{{ route('pertanyaan.create')}}" role="button">Tambah data</a><br><br>
                <table class="table table-bordered" id="pertanyaan">
                  <thead>                  
                    <tr>
                      <th style="width: 10px">No</th>
                      <th>judul</th>
                      <th>isi</th>
                      <th>tanggal diperbaharui</th>
                      <th style="width: 40px">Label</th>
                    </tr>
                  </thead>
                  <tbody>
                  @forelse($pertanyaan as $key => $pertanyaan)
                    <tr>
                        <td>{{$key + 1}}</td>
                        <td>{{$pertanyaan->judul}}</td>
                        <td>{{$pertanyaan->isi}}</td>
                        <td>{{$pertanyaan ->tanggal_diperbaharui}}</td>
                        <td style="display:flex;">
                            <a href="{{route('pertanyaan.show', ['pertanyaan' => $pertanyaan->id])}}" class="btn btn-info"><i class="fa fa-info"></i></a>
                            <a href="{{route('pertanyaan.edit', ['pertanyaan' => $pertanyaan->id])}}" class="btn btn-default"><i class="fa fa-edit"></i></a>
                            <form action="{{route('pertanyaan.destroy', ['pertanyaan' => $pertanyaan->id])}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <input type="submit" value="DELETE" class="btn btn-danger">
                            </form>
                        </td>
                    </tr>
                  @empty
                    <tr>
                        <td colspan="5" align="center">tidak ada postingan</td>
                    </tr>
                 @endforelse
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <!-- <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                  <li class="page-item"><a class="page-link" href="#">«</a></li>
                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                  <li class="page-item"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item"><a class="page-link" href="#">»</a></li>
                </ul>
              </div> -->
            </div>
@endsection


@push('js')
<script src="../../plugins/datatables/jquery.dataTables.js"></script>
<script src="../../plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
  $(function () {
    $("#pertanyaan").DataTable();
  });
</script>

@endpush